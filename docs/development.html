<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta http-equiv="Content-Style-Type" content="text/css" />
  <meta name="generator" content="pandoc" />
  <title></title>
  <style type="text/css">code{white-space: pre;}</style>
  <link rel="stylesheet" href="buttondown.css" type="text/css" />
</head>
<body>
<h1 id="developing-puppet-providers-with-augeasproviders">Developing Puppet providers with <code>augeasproviders</code></h1>
<p>The <code>augeasproviders</code> library is a helper Ruby library to write Puppet providers allowing to manage files in parts, using Augeas.</p>
<p>This page describes the steps to create a provider using the <code>augeasproviders</code> API.</p>
<h2 id="framework">Framework</h2>
<div class="figure">
<img src="images/library_stack.png" title="The library stack" alt="library_stack" /><p class="caption">library_stack</p>
</div>
<h2 id="puppet-type">Puppet type</h2>
<p>The first thing you need is a Puppet type. It could be an existing type or a new type.</p>
<p>Whether you use an existing type or create a new one, the type should:</p>
<ul>
<li>be ensurable</li>
<li>have a <code>target</code> parameter, specifying which file to manage.</li>
</ul>
<h2 id="making-use-of-the-library-in-your-provider">Making use of the library in your provider</h2>
<p>The first thing you need to do is to load the <code>AugeasProviders::Provider</code> module in your provider code.</p>
<p>First, add a <code>require</code> statement on the library at the top of your provider file:</p>
<pre><code>require File.dirname(__FILE__) + &#39;/../../../augeasproviders/provider&#39;</code></pre>
<p>Or, if your provider resides in a different module than <code>augeasproviders</code>:</p>
<pre><code>require File.dirname(__FILE__) + &#39;/../../../../../augeasproviders/lib/augeasproviders/provider.rb&#39;</code></pre>
<p>Then, include the module in your provider declaration, for example:</p>
<pre><code>Puppet::Type.type(:my_type).provide(:augeas) do
  desc &quot;Uses Augeas API to update my file&quot;

  include AugeasProviders::Provider</code></pre>
<h2 id="declaring-the-default-target">Declaring the default target</h2>
<p>You can declare the default target for your provider by using the <code>default_file</code> method:</p>
<pre><code>default_file do
  &quot;/path/to/my/file&quot;
end</code></pre>
<p>This method takes a block which is interpreted when the default file is needed. Consequently, you can use code in the block, basing yourself on informations such as facts (using the <code>facter</code> API) to determine the default file.</p>
<p>The default file path will be used to declare a <code>$target</code> Augeas variable, which you can then use in your Augeas paths whenever needed.</p>
<h2 id="declaring-the-lens-to-use">Declaring the lens to use</h2>
<p>The next thing <code>augeasproviders</code> needs to know if the lens to use on this file. You can declare it with the <code>lens</code> method:</p>
<pre><code>lens do
  &quot;MyFile.lns&quot;
end</code></pre>
<p>Again, the block is interpreted down the road so you can use code in this block.</p>
<p>In the event that you need information from the resource itself in order to determine which lens to use, the method can yield it to you. For example, if you have a <code>lens</code> parameter in your type, you can use:</p>
<pre><code>lens do |resource|
  resource[:lens]
end</code></pre>
<h2 id="confining-your-provider">Confining your provider</h2>
<p>Since the <code>augeasproviders</code> library uses Augeas, it is safe to confine your provider so it is only considered by Puppet if the <code>augeas</code> feature is available. You can do this with:</p>
<pre><code>confine :feature =&gt; :augeas</code></pre>
<h2 id="declaring-the-resource-path">Declaring the resource path</h2>
<p>The <code>augeasproviders</code> library can take care of automatically declaring some provider methods if you specify the path to your resource in the Augeas tree. In order to do that, use the <code>resource_path</code> method, which yields the resource. You should use the <code>$target</code> Augeas variable (set by the <code>default_file</code> method) to refer to the root of the file you are managing, for example:</p>
<pre><code>resource_path do |resource|
  &quot;$target/#{resource[:name]}&quot;
end</code></pre>
<p>Using the <code>resource_path</code> method will automatically declare two provider methods:</p>
<ul>
<li><code>exists?</code> (which checks if the resource path exists)</li>
<li><code>destroy</code> (which removes the resource path and saves the tree)</li>
</ul>
<p>These two methods can be overridden in your provider if you need a more advanced behaviour.</p>
<p>The <code>resource_path</code> method is also used to define a <code>$resource</code> Augeas variable, which you can use in your Augeas expressions, alongside the <code>$target</code> variable.</p>
<h2 id="manipulating-the-augeas-tree">Manipulating the Augeas tree</h2>
<p>When defining your provider methods, you will need to manipulate the Augeas tree. <code>augeasproviders</code> provides two useful methods for this: <code>augopen</code> and <code>augopen!</code></p>
<p>The easiest way to use the <code>augopen</code> method is to pass it a block. It will then yield the Augeas handler to the block:</p>
<pre><code>augopen do |aug|
  aug.get(&#39;$resource&#39;)
end</code></pre>
<p>The <code>augopen</code> method will open Augeas with your file/lens combination alone (making it faster), safely manage Augeas errors, and close the Augeas handler at the end of the block.</p>
<p>If you need to perform a tree change with Augeas, the <code>augopen!</code> method behaves just like <code>augopen</code>, but saves the tree automatically:</p>
<pre><code>augopen! do |aug|
  aug.set(&#39;$resource&#39;, &#39;value&#39;)
end</code></pre>
<h2 id="defining-provider-methods">Defining provider methods</h2>
<p>One convenient way to declare a provider method which only calls Augeas to get or set in the tree is to use the <code>define_aug_method</code> or <code>define_aug_method!</code> methods:</p>
<pre><code>define_aug_method!(:destroy) do |aug, resource|
  aug.rm(&quot;$target/command[#{resource[:name]}]&quot;)
end</code></pre>
<p>Again, the <code>define_aug_method!</code> method will save the tree, while <code>define_aug_method</code> will not.</p>
<p>You can use the <code>define_aug_method!</code> method to define the <code>create</code> method, which is required for ensurable types and is not automatically created by Augeasproviders, e.g.:</p>
<pre><code>define_aug_method!(:create) do |aug, resource|
  aug.set(&quot;$target/#{resource[:name]}&quot;, resource[:value])
end</code></pre>
<h2 id="defining-property-accessors">Defining property accessors</h2>
<p><code>define_aug_method</code> lets you define generic methods for your provider. For ensurable types, properties need two methods, for getting and setting the property value respectively. The Augeasproviders library helps you to do that by providing property accessor methods.</p>
<p>In all the examples below, <code>$resource</code> will map to a <code>resource</code> node with value <code>name</code>, represented as:</p>
<pre><code>{ &quot;resource&quot; = &quot;name&quot; }</code></pre>
<h3 id="simple-accessor">Simple accessor</h3>
<p>The simplest way to define a property accessor is:</p>
<pre><code>attr_aug_accessor(:foo)</code></pre>
<p>Given <code>foo =&gt; &quot;bar&quot;</code>, this will set the tree to:</p>
<pre><code>{ &quot;resource&quot; = &quot;name&quot;
  { &quot;foo&quot; = &quot;bar&quot; } }</code></pre>
<p>This will manage a property called <code>foo</code>, whose value is stored as the value for the <code>foo</code> node of the <code>$resource</code> node.</p>
<p>Calling <code>attr_aug_accessor</code> defines both a reader and a writer methods, and is thus equivalent to calling:</p>
<pre><code>attr_aug_reader(:foo)
attr_aug_writer(:foo)</code></pre>
<p>These calls produce dynamic methods, named after the property you wish to control. In this case, they will be named respectively <code>attr_aug_reader_foo</code> and <code>attr_aug_writer_foo</code>. The standard ensurable methods <code>foo</code> and <code>foo=</code> are then automatically defined using these respective methods.</p>
<p>When calling these methods, the reader method takes an Augeas handler as parameter and returns the value as found in the file:</p>
<pre><code>value = attr_aug_reader_foo(aug)</code></pre>
<p>while the writer method takes an Augeas handler and a value, and sets the value in the file:</p>
<pre><code>attr_aug_writer(aug, value)</code></pre>
<h3 id="accessor-options">Accessor options</h3>
<p>The case where a resource property maps directly to a sub-node in the tree with the same name and a simple value is not always met. For more complex situations, the accessor methods accept a series of options.</p>
<h4 id="property-label">Property label</h4>
<p>Very often, the name of the property you want to manage does not match the node label in the Augeas tree. The <code>:label</code> option lets you set this, e.g. for <code>foo =&gt; 'value'</code>:</p>
<pre><code>attr_aug_access(:foo,
  :label =&gt; &#39;my/foo&#39;
)</code></pre>
<p>will manage a simple entry as:</p>
<pre><code>{ &quot;resource&quot; = &quot;name&quot;
  { &quot;my&quot; { &quot;foo&quot; = &quot;value&quot; } } }</code></pre>
<p>If the node to be used is the resource node itself instead of a sub-node, you can use <code>:resource</code> as the label, e.g.:</p>
<pre><code>attr_aug_access(:foo,
  :label =&gt; :resource
)</code></pre>
<p>will manage the entry as:</p>
<pre><code>{ &quot;resource&quot; = &quot;value&quot; }</code></pre>
<h4 id="property-type">Property type</h4>
<p>The property type defines the type of value to be managed, and can be one of the following values:</p>
<ul>
<li><code>:string</code>: the value is a string (default)</li>
<li><code>:array</code>: the value is an array</li>
<li><code>:hash</code>: the value is a hash</li>
</ul>
<h5 id="string-value">String value</h5>
<p>When the value is set as a string, the reader method returns the value of the node referred to, and the writer method sets the value of the node if a value is given, or clears it otherwise, e.g. for <code>foo =&gt; &quot;bar&quot;</code>:</p>
<pre><code>attr_aug_accessor(:foo,
  :type =&gt; :string
)</code></pre>
<p>maps to:</p>
<pre><code>{ &quot;resource&quot; = &quot;name&quot;
  { &quot;foo&quot; = &quot;bar&quot; } }</code></pre>
<p>If you need the sub-node to be removed rather than cleared when the value is set to <code>nil</code> (or not set), use the <code>:rm_node</code> option:</p>
<pre><code>attr_aug_accessor(:foo,
  :type =&gt; string,
  :rm_node =&gt; true
)</code></pre>
<h5 id="array-value">Array value</h5>
<p>Augeas has two ways of representing array values in its trees, using either fix labels or sequential entries (see <a href="http://www.redhat.com/archives/augeas-devel/2011-February/msg00053.html">this page</a> for an explanation of why both of them exist).</p>
<p>For this reason, property accessors offer 3 ways to manage arrays, using the <code>sublabel</code> option:</p>
<ul>
<li><p>the values are all the nodes matching the path with the given label (<code>sublabel</code> not set), e.g. for <code>foo =&gt; [&quot;bar&quot;, &quot;baz&quot;]</code>:</p>
<pre><code>attr_aug_accessor(:foo,
  :type     =&gt; :array
)</code></pre>
<p>maps to:</p>
<pre><code>{ &quot;resource&quot; = &quot;name&quot;
  { &quot;foo&quot; = &quot;bar&quot; }
  { &quot;foo&quot; = &quot;baz&quot; } }</code></pre></li>
<li><p>the values are sub-nodes of the path with the given label (<code>sublabel</code> set to the label of the sub-nodes), e.g. for <code>foo =&gt; [&quot;bar&quot;, &quot;baz&quot;]</code>:</p>
<pre><code>attr_aug_accessor(:foo,
  :type     =&gt; :array,
  :sublabel =&gt; &#39;sub&#39;
)</code></pre>
<p>maps to:</p>
<pre><code>{ &quot;resource&quot; = &quot;name&quot;
  { &quot;foo&quot;
    { &quot;sub&quot; = &quot;bar&quot; }
    { &quot;sub&quot; = &quot;baz&quot; } } }</code></pre></li>
<li><p>the values are sequential entries under the path with the given label (<code>sublabel</code> set to <code>:seq</code>), e.g. for <code>foo =&gt; [&quot;bar&quot;, &quot;baz&quot;]</code>:</p>
<pre><code>attr_aug_accessor(:foo,
  :type     =&gt; :array,
  :sublabel =&gt; :seq
)</code></pre>
<p>maps to:</p>
<pre><code>{ &quot;resource&quot; = &quot;name&quot;
  { &quot;foo&quot;
    { &quot;1&quot; = &quot;bar&quot; }
    { &quot;2&quot; = &quot;baz&quot; } } }</code></pre></li>
</ul>
<p>In all cases, all existing values are purged before setting the target values.</p>
<h5 id="hash-value">Hash value</h5>
<p>In the Augeas tree, hash values are represented by sub-nodes, with optional values (the <code>:sublabel</code> option is used to set the value node name). When no value is found in the tree, the accessor method will default to the value of the <code>:default</code> option.</p>
<p>For example, given <code>foo =&gt; { &quot;a&quot; =&gt; &quot;bar&quot;, &quot;b&quot; =&gt; &quot;baz&quot; }</code>, with:</p>
<pre><code>attr_aug_accessor(:foo
  :type     =&gt; :hash,
  :default  =&gt; &quot;baz&quot;,
  :sublabel =&gt; &quot;val&quot;
)</code></pre>
<p>will produce:</p>
<pre><code>{ &quot;resource&quot; = &quot;name&quot;
  { &quot;foo&quot;
    { &quot;a&quot;
      { &quot;val&quot; = &quot;bar&quot; } }
    { &quot;b&quot; # No value here because &quot;baz&quot; is the default value
      } } }</code></pre>
<p>Note that only one level of hash depth is currently supported.</p>
</body>
</html>
